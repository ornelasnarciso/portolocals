const CleanCSS = require("clean-css");
const htmlmin = require("html-minifier");
const eleventyPluginFilesMinifier = require("@sherby/eleventy-plugin-files-minifier");
const lazyImagesPlugin = require("eleventy-plugin-lazyimages");
const fs = require("fs");
const { minify } = require("terser"); // javascript minify

const pluginRss = require("@11ty/eleventy-plugin-rss"); //RSS feed
const pluginSEO = require("eleventy-plugin-seo"); // SEO - also provides social preview images
const yaml = require("js-yaml"); // use yaml files

const md = require("markdown-it")({
  html: false,
  breaks: true,
  linkify: true,
}); // transform yaml files in markdown

module.exports = function (cfg) {
  cfg.addPassthroughCopy("styles");
  cfg.addPassthroughCopy("images");
  cfg.addPassthroughCopy("js");
  cfg.addPassthroughCopy("video");
  cfg.addPassthroughCopy("admin");
  cfg.addPlugin(pluginRss); //RSS feed
  cfg.addDataExtension("yaml", (contents) => yaml.safeLoad(contents)); // use yaml files
  cfg.addNunjucksFilter("markdownify", (markdownString) =>
    md.render(markdownString)
  ); // transform yaml files in markdown

  cfg.addPlugin(pluginSEO, {
    title: "Porto Locals",
    description: "Porto Locals",
    url: "https://portolocals.com",
    author: "Porto Locals",
  });

  // javascript minify
  cfg.addNunjucksAsyncFilter("jsmin", async function (code, callback) {
    try {
      const minified = await minify(code);
      callback(null, minified.code);
    } catch (err) {
      console.error("Terser error: ", err);
      // Fail gracefully.
      callback(null, code);
    }
  });

  // html minify
  cfg.addTransform("htmlmin", (content, outputPath) => {
    if (outputPath.endsWith(".html")) {
      return htmlmin.minify(content, {
        collapseWhitespace: true,
        removeComments: true,
        useShortDoctype: true,
      });
    }

    return content;
  });
  cfg.addFilter("cssmin", function (code) {
    return new CleanCSS({}).minify(code).styles;
  });
  cfg.addPlugin(eleventyPluginFilesMinifier);
  cfg.addPlugin(lazyImagesPlugin);

  // 404
  cfg.setBrowserSyncConfig({
    callbacks: {
      ready: function (err, bs) {
        bs.addMiddleware("*", (req, res) => {
          const content_404 = fs.readFileSync("_site/404.html");
          res.write(content_404);
          res.writeHead(404);
          res.end();
        });
      },
    },
  });
};
